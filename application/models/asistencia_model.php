<?php

if (!defined('BASEPATH'))
    exit('<h1>No se permite el acceso directo al script</h1>');

class Asistencia_model extends CI_Model {

	public function getAsignaturasDocentexCurso($bhv){
		$ssql = "SELECT acaasignaturadocente.id AS idact
        , acaasignaturas.codigo AS cod_asignatura, acaasignaturas.nombre AS asignatura, acaplanestudios.nombre AS programa
        , CONCAT_WS(' ',acasemestres.nombre, acagrupos.nombre) AS curso_grupo, acasemestres.orden AS orden_semestre
        	FROM acaasignaturadocente
        INNER JOIN acaplanestudiosasignaturas ON (acaasignaturadocente.planestudiosasignatura_id = acaplanestudiosasignaturas.id)
        INNER JOIN acaasignaturas ON (acaplanestudiosasignaturas.asignaturas_id = acaasignaturas.id)
        INNER JOIN acaperiodos ON (acaasignaturadocente.periodos_id = acaperiodos.id AND acaperiodos.activo=1)
        INNER JOIN acaplanestudios ON (acaplanestudiosasignaturas.planestudios_id = acaplanestudios.id)
        INNER JOIN acasemestres ON (acaplanestudiosasignaturas.semestre_id = acasemestres.id)
        INNER JOIN acagrupos ON (acaasignaturadocente.grupos_id = acagrupos.id)
        INNER JOIN bancohojasvida ON (bancohojasvida.id=acaasignaturadocente.bancohojasvida_id)
        INNER JOIN personas ON (personas.id=bancohojasvida.personas_id)
        #INNER JOIN secusers ON (secusers.personas_id=personas.id)
        WHERE personas.id = 1 ORDER BY acasemestres.orden ASC, acasemestres.id ASC, acagrupos.nombre ASC, acaasignaturas.nombre ASC";

        return $this->db->query($ssql)->result_array();

	}

    public function getFiltros($id_asigdoc){
        $ssql = "SELECT acaasignaturadocente.id, CONCAT_WS('-',acasemestres.nombre, acagrupos.abrev) AS curso, acaplanestudiosasignaturas.semestre_id AS semestre_id, acaasignaturadocente.grupos_id AS grupo_id, acaasignaturadocente.periodos_id AS periodo_id, acaplanestudiosasignaturas.planestudios_id AS plan_id, acaperiodos.descripcion AS periodo, acaplanestudios.nombre AS plan, acaasignaturas.nombre AS asignatura
            FROM acaasignaturadocente
        INNER JOIN acaplanestudiosasignaturas ON (acaplanestudiosasignaturas.id = acaasignaturadocente.planestudiosasignatura_id)
        INNER JOIN acaperiodos ON (acaperiodos.id = acaasignaturadocente.periodos_id)
        INNER JOIN acasemestres ON (acasemestres.id = acaplanestudiosasignaturas.semestre_id)
        INNER JOIN acagrupos ON (acagrupos.id = acaasignaturadocente.grupos_id)
        INNER JOIN acaplanestudios ON (acaplanestudios.id = acaplanestudiosasignaturas.planestudios_id)
        INNER JOIN acaasignaturas ON (acaasignaturas.id = acaplanestudiosasignaturas.asignaturas_id)
        WHERE acaasignaturadocente.id = {$id_asigdoc}";

        return $this->db->query($ssql)->row_array();
    }

    public function getEstudiantes($filtros){
        $ssql = "SELECT personas.id AS persona_id, acamatricula.id AS matricula_id, acamatriculaasignatura.id AS matasig_id, personas.identificacion, CONCAT_WS(' ', personas.apellido1, personas.apellido2, personas.nombre1, personas.nombre2) AS nomape
            FROM acamatriculaasignatura
        INNER JOIN acamatricula ON (acamatricula.id = acamatriculaasignatura.matricula_id)
        INNER JOIN acainscripcion ON (acainscripcion.id = acamatricula.inscripcion_id)
        INNER JOIN personas ON (personas.id = acainscripcion.persona_id)
        WHERE acamatricula.semestre_id = {$filtros['semestre_id']} AND acamatricula.grupo_id = {$filtros['grupo_id']} AND acamatricula.periodo_id = {$filtros['periodo_id']} AND acainscripcion.planestudios_id = {$filtros['plan_id']}";

        return $this->db->query($ssql)->result_array();

    }

    public function getEstudiante($ide_per="", $cod_per=""){
        $aux = ($ide_per != '') ? "personas.identificacion = {$ide_per}" : "acamatricula.codigo = {$cod_per}";
        $sql = "SELECT acamatriculaasignatura.id AS matasig_id,  acamatriculaasignatura.* FROM acamatriculaasignatura
            INNER JOIN acamatricula ON acamatricula.id = acamatriculaasignatura.matricula_id
            INNER JOIN acainscripcion ON acainscripcion.id = acamatricula.inscripcion_id
            INNER JOIN personas ON personas.id = acainscripcion.persona_id
            WHERE ".$aux."";

        return $this->db->query($sql)->result_array();
    }
}