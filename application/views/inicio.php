<!DOCTYPE html>
<html>
<head>
	<title>Asistencia Biometrica</title>
	<!-- bootstrap & fontawesome -->
    <script src="<?= base_url(); ?>resources/assets/js/jquery.js"></script>
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/bootstrap.css" />

    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/font-awesome.min.css" />

    <!-- text fonts -->
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/ace-fonts.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>resources/assets/css/jquery.gritter.css">

    <!-- ace styles -->
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/ace.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/ace-rtl.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/ace.onpage-help.css" />
	<script type="text/javascript" language="javascript" src="<?= base_url(); ?>resources/assets/js/jquery.gritter.js"></script> 
</head>
<body>
<div class="container">
	<div style="background-color: #34495E" id="navbar" class="navbar navbar-default">
	    <div class="navbar-container" id="navbar-container">
	    	<div class="navbar-header pull-left help-section hidden-xs" data-id-help="1">
	    		<img src="<?= base_url(); ?>img/logo-ITP.png" class="no-margin no-padding pull-left hidden-xs" style="max-width:35px;">
	    		<span class="text-center" style="text-align: center; font-size: 18px">INSTITUTO TECNOLOGICO DEL PUTUMAYO</span>	    		
	    	</div>

	    	<div class="navbar-buttons navbar-header pull-right" role="navigation">
				<li class="dark help-section" data-id-help="126">
	                <a data-toggle="dropdown" class="dropdown-toggle bs_a_enlaces" href="<?= base_url() . "app/salir"; ?>" title="Cerrar Sesion">
	                	<span style="color: white; font-size: 16px"><?php echo $this->session->userdata('nomape'); ?></span> 

	                    <span class="badge badge-important" id="badg_anuns"><i class="ace-icon fa fa-power-off bigger-100 "></i></span>
	                </a>
	            </li>
			</div>
		</div>
	</div>
	<div class="row">
	    <div id="espacio_trabajo" class="col-lg-12">
	        <div id="contenedor" class="row">

	        </div>

	        <div id="form_content" class="row">

	        </div>
	    </div>
	</div>
</div>
</body>
	<footer class="footer">
        <div class="footer-inner">
            <div class="footer-content">
        		<p style="line-height: 1; background-color: #34495E; height: 55; margin-top: -10px; color: white; padding:5px 0px; clear: both;">
				Instituto Tecnologico del Putumayo<br>
				Facultad de Ingenieria y Ciencias Basicas<br>
				Ingenieria de Sistemas
				</p>            	
            </div>
        </div>
	</footer>
</html>
<style type="text/css">
    .navbar{
    	color: #fff;
    }

</style>
<script type="text/javascript">
	$(document).ready(function(){

        var height = $(window).height();
        $('#espacio_trabajo').height(height);
	});

	$(function () {
		$.ajax({
		    url: "<?=base_url();?>asistencia/menu", 
		    type: 'POST',
		    success: function (data) {
		        $("#contenedor").html(data);
		    }
		});
	});
</script>