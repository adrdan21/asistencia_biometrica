<div class="container">
    <div class="row">
        <div class="col-lg-9 col-md-8 col-sm-6 hidden-xs">
            <div class="well well-sm">
                <span class="h4 green">Escoge la asignatura para registrar asistencia biometrica.</span>
            </div>
        </div>
        <div class="col-xs-12 col-lg-3 col-md-4 col-sm-6">
            <b>Filtrar Grupo:</b>
            <select id="filter_grupo_lista_docente">
                <option value="">Ninguno</option>
                <?php foreach ($grupos as $value) { ?>
                    <option value="<?= str_replace(' ', '_', $value) ?>"><?= $value ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <?php foreach ($asignaturas as $row) { ?>
        <div class="col-sm-6 col-md-4 col-lg-3 col-xs-12 <?= str_replace(' ', '_', $row["curso_grupo"]) ?> class_all">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <span class="panel-title"><?= $row["curso_grupo"]; ?></span>
                </div>
                <div class="panel-body no-padding">
                    <ul class="item-list">
                        <li class="item-orange" data-asig_id="<?= $row["idact"]; ?>" style="cursor: pointer;" > 
                            <span class="blue h5"><?= $row["asignatura"]; ?> </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <?php } ?>
</div>


<script>

    $('#filter_grupo_lista_docente').on('change', function () {
        if ($(this).val() == "") {
            $('.class_all').removeClass('hide');
        }else{
            $('.class_all').addClass('hide');
            $('.'+$(this).val()).removeClass('hide');
        }
    });

    $('.item-orange').on('click', function () {
        let asigdoc_id = $(this).attr('data-asig_id');
        $("#form_content").load("<?=base_url();?>asistencia/get_Estudiantes_sin_Huella/" + asigdoc_id);
        IrForm();
    });

    function IrForm() {
        $(document).unbind('keyup');
        document.title = 'App';
        $("#contenedor").hide();
        $("#form_content").show("slide", {direction: "down"}, 500);
        var self = $("body");
        //MinimizarChats();
    }

</script>
