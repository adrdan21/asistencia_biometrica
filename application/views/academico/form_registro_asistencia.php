<style>
    .noFormalBtn{
        margin:auto;
    }
    .noFormalBtn:hover{
        opacity: 0.75;
        cursor:pointer;
    }
    .lector{
    	margin-left: auto;
    	margin-right: auto;
    	border: 1px;
    }
    img{
		display:block;
		margin:auto;
	}
	thead{
		background-color: #34495E;
		color: white;
	}
	th{
		padding-left: 10px;
	}
	td{
		padding-left: 10px;
	}


</style>
<div class="container">
	<div class="widget-box widget-color-dark ui-sortable-handle light-border">
        <div class="widget-header widget-header-small">
            Sistema Biometrico de Asistencia.
            <div class="btn-group btn-corner">
            	<button class="btn btn-xs btn-primary" onclick="activar_sesion()" type="button">
                    <i class="ace-icon fa fa-reorder"></i>
                    Habilitar Sesión
            	</button>
            </div>
            
            <div class="btn-group btn-corner pull-right">
                <button class="btn btn-xs btn-success btnGuardarClase" type="button">
                    <i class="ace-icon fa fa-check"></i>
                    Guardar
                </button>
                <button class="btn btn-xs btn-default" type="button" onclick="VolverForm()">
                    <i class="fa fa-reply"></i>
                    Cancelar
                </button>
            </div>
        </div>
        <div class="widget-body widget-body-small widget-color-white">
        	<table width="100%">
        		<tr>
        			<td width="40%"><b>Programa:</b> <?=$filtros['plan'] ?></td>
        			<td width="30%"><b>Asignatura:</b> <?=$filtros['asignatura'] ?></td>
        			<td width="20%"><b>Semestre:</b> <?=$filtros['curso'] ?></td>
        			<td width="20%"><b>Periodo:</b> <?=$filtros['periodo'] ?></td>
        		</tr>
        	</table>
        </div>
    </div>
	<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-6 lector text-center">
				<h3>Letor de Huella</h3>
                <img class="img-responsive" align="center" src="<?= base_url(); ?>img/huella.jpg" alt="Cargando">
                <div class="alert alert-info hide" id="msgbox"></div>
            </div>			
			<div class="col-sm-6">
				<h3>Registro por código</h3>
				<table id="buscador_app" class="clase_table" width="100%">
					<tr class="text-center">
						<th>Ingresar Codigo</th>
						<td>
							<input type="text" name="codigo" id="codigo" value="" placeholder="Digitar Codigo" class="gui-input" autofocus>
						</td>
					</tr>
					<tr class="text-center">
						<th>Ingresar Cedula</th>
						<td>
							<input type="text" name="cedula" id="cedula" value="" placeholder="Digitar Cedula">
						</td>
					</tr>
				</table>
				<div class="col-sm-12">
					<button type="button" class="btn btn-primary" onclick="validar_codigo()">Validar</button>	
				</div>
				
			</div>
		</div>
	</div>
	<hr/>
	<div class="row">
		<div class="col-sm-12">
			<table width="100%" align="center">
				<thead class="thead-dark">
					<tr >
						<th style ="vertical-align:text-top;" rowspan="2">#</th>
						<th style ="vertical-align:text-top;" rowspan="2">Identificacion</th>
						<th style ="vertical-align:text-top;" rowspan="2">Nombres y Apellidos</th>
						<th class="text-center" colspan="2">Asistencia</th>
					</tr>
					<tr>
						<th class="text-center">Asistió</th>
						<th class="text-center">Justificó</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$contador = 0;
					foreach ($estudiantes as $row) {
						$contador++;
						?>
						<tr>
							<th><?=$contador; ?></th>
							<td><?=$row['identificacion']; ?> </td>
							<td><?=$row['nomape']; ?> </td>
							<td class="text-center">
								<span  class="icon_asistencia" id="icon_asistencia_<?=$row['matasig_id'];?>"><i class="ace-icon fa fa-hand-paper-o bigger-150 "></i></span>
								<input type="hidden" id="faltas" name="faltas" data-id="<?=$row['matasig_id'];?>">
								
							</td>
							<td class="text-center"> 
								<input type="checkbox" id="falta_just" name="falta_just" data-id="<?=$row['matasig_id'];?>">								
							</td>
						</tr>
					<?php }
					?>
					
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">

	/*var validar_falta = true;

	if(validar_falta == true){
		$("#icon_asistencia").attr("class", "badge badge-success");
	}*/

	function VolverForm() {
	    $(document).unbind('keyup');
	    $("#form_content").hide();
	    $("#form_content").empty();
	    $("#contenedor").show("slide", {direction: "down"}, 500);
	    var self = $("body");
	}

	function validar_codigo(){
		var datos = {
            cc_usuario : $('#cedula').val(),
            cod_usuario : $('#codigo').val()
        }
        $.ajax({
           	url:"<?=base_url();?>asistencia/getEstudiante",
            data: datos, 
            type: 'POST', dataType: 'json',
            success: function (data) {
               	var class_not = "";
                if (data != '') {
                	var matasig_id = data[0].matasig_id;
                	$("#icon_asistencia_"+ data[0].matasig_id).attr("class", "badge badge-success");
                    class_not = 'gritter-success';
                    jQuery.gritter.add({title: 'Sistema', text: 'Usuario localizado satisfactoriamente', time: '3000', class_name: class_not});
                    //setTimeout('GuardarVisita()',5000);
                } else {
                    class_not = 'gritter-error';
                    jQuery.gritter.add({title: 'Sistema', text: 'Usuario no localizado', time: '3000', class_name: class_not});
                }
                
            }
        }); 
    }

</script>
