<style>
    .noFormalBtn{
        margin:auto;
    }
    .noFormalBtn:hover{
        opacity: 0.75;
        cursor:pointer;
    }
</style>

<div class="row hidden-xs"><br><br></div>
<div class="col">
	<div class="row">
        <div class="col-lg-12 col-lg-offset-3">
			<div class="row">
		        <div class="col-lg-3 ">
		            <div class="form-group">
		                <div class="noFormalBtn" onclick="redirFromSquareBtn(this)" data-to="asistencia/listar_grupos">
		                    <img class="img-responsive" src="<?= base_url(); ?>img/botones/1.jpg" alt="Cargando">
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>


<script type="text/javascript">

	function redirFromSquareBtn(elem) {

	    let url = elem.getAttribute("data-to");
	    if (url.length > 0) {
	        $.ajax({
	            url: "<?= base_url();?>" + url,
	            type: 'POST',
	            success: function (data) {
	                $("#contenedor").html(data);
	            }
	        });
	    }
	}
</script>
