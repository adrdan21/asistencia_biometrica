<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Asistencia Biometrica</title>
    <script src="<?= base_url(); ?>resources/assets/js/jquery.js"></script>
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/bootstrap.css" />

    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/font-awesome.min.css" />

    <!-- text fonts -->
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/ace-fonts.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>resources/assets/css/jquery.gritter.css">

    <!-- ace styles -->
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/ace.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/ace-rtl.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>resources/assets/css/ace.onpage-help.css" />
	<script type="text/javascript" language="javascript" src="<?= base_url(); ?>resources/assets/js/jquery.gritter.js"></script> 

</head>
<body>
	<div class="main-container">
        <div class="main-content" id="view_login_inicio">
            <div class="row hidden-xs">
                <br>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="login-container">
                        <div class="space-6"></div>
                        <div class="position-relative">
                            <div id="login-box" class="login-box visible widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">

                                        <div class="row">
                                            <h4 class="header blue lighter bigger center">
                                                Iniciar Sesión.
                                            </h4>
                                        </div>

                                        <div class="space-6"></div>

                                        <form id="form_login">
                                            <fieldset>
                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" id="txt_usuario" class="form-control" name="usu" placeholder="Usuario"/>
                                                        <i class="ace-icon fa fa-user"></i>
                                                    </span>
                                                </label>

                                                <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="password" id="txt_passw" class="form-control" name="pass" placeholder="Contraseña"/>
                                                        <i class="ace-icon fa fa-lock"></i>
                                                    </span>
                                                </label>

                                                <div class="space"></div>
                                                <div class="clearfix center">
                                                    <a href="#" onclick="login()" id="btn_login" class="btn btn-block btn-primary btn-round">
                                                        <span class="bigger-110">INICIAR SESIÓN</span>
                                                    </a>
                                                    
                                                </div>
                                                <div class="space-4"></div>
                                                <div class="alert alert-info hide" id="msgbox">

                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script type="text/javascript">

	function login(){ 
        
        /*Dvn*/
        $("#msgbox").attr("class", "alert alert-info");
        $("#msgbox").html("<span> Autenticando...<span>");
        $.ajax({
            //url: "<?=base_url();?>login/validar_login",
            url: "<?=base_url();?>app/validar_login",
            data: $("#form_login").serialize(),
            dataType: 'json', type: 'POST',
            success: function (data) {
                if (data.ok) {

                    $("#msgbox").attr("class", "alert alert-success");
                    $("#msgbox").html("<span>" + data.msg + "<span>");

                    url_2 = "<?=base_url();?>app/app";
                    $(location).attr('href',url_2);

                } else {
                    $("#msgbox").attr("class", "alert alert-danger");
                    $("#msgbox").html("<span> Verifique su Usuario o Contraseña.<span>");
                }
            }
        });
        return false;
    };
</script>