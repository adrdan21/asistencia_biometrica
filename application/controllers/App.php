<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model("login_model", "md_log");
    }

	public function index(){
		$this->load->view('login');
	}

	public function validar_login() {

        $usuario = $this->input->post("usu");
        $pssw = md5($this->input->post("pass"));

        $rta['datos'] = $this->md_log->validar_login($usuario, $pssw);

        if($rta['datos'] != false){
            $newdata['nomape'] = $rta['datos'][0]['nomape'];
            $newdata['persona_id'] = $rta['datos'][0]['id'];
            $newdata['is_login'] = true;
            $this->session->set_userdata($newdata);

        	$rta['ok'] = true;
        	$rta['msg'] = "Bienvenido ".$rta['datos'][0]['nomape']."";
            //$rta['msg'] = $this->session->userdata('nomape');
 ;
        }else{
        	$rta['ok'] = false;
        }
        echo json_encode($rta);
    }

    public function salir() {
        $this->session->sess_destroy();
        redirect("localhost/asistencia_biometrica");
    }

    public function app(){
        $this->load->view('inicio');
    }

}
