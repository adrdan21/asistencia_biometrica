<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asistencia extends CI_Controller {

	function __construct() {
        parent::__construct();
        $Login = $this->session->userdata('is_login');
        if(!$Login){
            redirect();
        }
        $this->load->model("asistencia_model", "md_asis");
    }

    public function menu(){
        $this->load->view('academico/menu_principal');
    }

	public function listar_grupos(){
        $persona_id = $this->session->userdata('id');
        $dtsVista["asignaturas"] = $this->md_asis->getAsignaturasDocentexCurso($persona_id);
        $dtsVista["grupos"] = array_unique(array_column($dtsVista["asignaturas"], 'curso_grupo'));
        $this->load->view('academico/listar_asignaturas_docente', $dtsVista);
        //var_dump($dtsVista);
	}

    public function get_Estudiantes_sin_Huella($id_asigdoc){
        $filtros = $this->md_asis->getFiltros($id_asigdoc);
        $dtsVista['filtros'] = $filtros;
        $dtsVista["estudiantes"] = $this->md_asis->getEstudiantes($filtros);
        $this->load->view('academico/form_registro_asistencia', $dtsVista);
        //var_dump($dtsVista);
    }

    public function getEstudiante(){
        $cedula = $this->input->post('cc_usuario');
        $codigo = $this->input->post('cod_usuario');

        $res = $this->md_asis->getEstudiante($cedula, $codigo);

        echo json_encode($res);
    }
}
