<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Sess_Controller extends CI_Controller {

	function __construct() {
        parent::__construct();
        $Login = $this->session->userdata('is_login');
        if(!$Login){
        	$this->load->view('login');
        }
    }
	
}