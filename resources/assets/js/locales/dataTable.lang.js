$.extend({
    "lang": function (Lang) {
        var jLang;
        switch (Lang) {
            case 'dataTable.es':
                jLang = {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron registros.",
                    "sEmptyTable": "Usa los filtros o la busqueda para visualizar los registros.",
                    "sInfo": "Visualizando _START_ a _END_ [Total _TOTAL_]",
                    "sInfoEmpty": "Registros 0",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                };
                break;
            default:
                jLang = null;
                break;
        }
        return jLang;
    }
});