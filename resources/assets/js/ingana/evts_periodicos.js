/* hyoG Rev 29112018 <= ESTO ESTA AFUERA POR QUE LA AGENDA ES GLOBAL, ESO CREO => */
var glo_sincro_periodico = "NO";
var glo_evts_peri;

function guardian_evts_periodicos(fec_periodizar) {
    if (glo_sincro_periodico == "NO") {
        $.ajax({url: rutaAbs + "administrativo/agenda/ajax_agenda_perio_usu", dataType: 'json', async: false
        }).done(function (data) {
            glo_evts_peri = data;
        });
        glo_sincro_periodico = "SI";
    }

    var mom_cal = moment(fec_periodizar);
    if (glo_evts_peri.length > 0) {
        var cad_ids = "";
        var cad_event = "";
        for (var j = 0; j < glo_evts_peri.length; j++) {
            var i = 0;
            cad_ids = cad_ids + "," + glo_evts_peri[j].id;
            var mom_periodizado = moment(glo_evts_peri[j].periodizado_hasta);
            var mom_periodizar = moment(glo_evts_peri[j].periodizar_hasta);

            var fut_ini = moment(glo_evts_peri[j].fechaini);
            while (moment(fut_ini.format('YYYY-MM-DD')).isBefore(mom_cal.endOf('month').format('YYYY-MM-DD'))) {
                i++;
                var inter = ((glo_evts_peri[j].intervalo * 1) == 0) ? 1 : (glo_evts_peri[j].intervalo * 1);
                var pivot = inter * i;
                var fut_ini = moment(glo_evts_peri[j].fechaini).add(pivot, glo_evts_peri[j].cod_fullcalendar);
                var fut_fin = moment(glo_evts_peri[j].fechafin).add(pivot, glo_evts_peri[j].cod_fullcalendar);
                if (fut_ini.format('YYYY') == mom_periodizado.format('YYYY') && (fut_ini.format('M') * 1) == (mom_periodizado.format('M') * 1) && (fut_ini.format('M') * 1) == (mom_cal.format('M') * 1) && fut_ini.isBefore(mom_periodizar)) {
                    cad_event = cad_event + "{ padre:'" + j + "', fi:'" + fut_ini.format('YYYY-MM-DD') + "', ff:'" + fut_fin.format('YYYY-MM-DD') + "', hi:'" + glo_evts_peri[j].horaini + "', hf:'" + glo_evts_peri[j].horafin + "'},";
                }
            }

            if (moment(mom_periodizado.startOf('month').format('YYYY-MM-DD')).isSame(mom_cal.startOf('month').format('YYYY-MM-DD'))) {
                glo_evts_peri[j].periodizado_hasta = mom_periodizado.endOf('month').add(1, 'd').format('YYYY-MM-DD');
            }
        }
        console.table(glo_evts_peri);
        console.log('=>'+cad_event);
        if (cad_event != "") {
            var eventos = eval("[" + cad_event + "]");
            $.ajax({
                url: rutaAbs + "administrativo/agenda/save_array_evts",
                type: 'POST', data: 'json_evts=' + JSON.stringify(eventos) + '&json_perios=' + JSON.stringify(glo_evts_peri) + '&fec=' + fec_periodizar
            });
        }
    }
}
guardian_evts_periodicos(moment().format('YYYY-MM-DD'));     