/* hyoG Rev 29112018 <= ESTO YA NO SE USA, MEJORAR CUANDO SE REACTIVE => */
function comprobar_biometria_hoy() {
    if ($('#badg_notis').length) {
        $.ajax({type: 'POST', dataType: 'json',
            url: rutaAbs + "administrativo/biometria/jornadas_usu_hoy",
            beforeSend: function () {
                $('.page-content').unblock();
            },
            success: function (cr) {
                if (cr != "") {
                    if ((cr[0].biometria * 1) == 1) {
                        var cad_html = "<b>Mañana:</b> <span class='badge badge-warning'>" + ((cr[0].entrada_manana == null) ? 'Pendiente' : cr[0].entrada_manana) + " - " + ((cr[0].salida_manana == null) ? 'Pendiente' : cr[0].salida_manana) + "</span><br>";
                        cad_html = cad_html + "<b>Tarde:</b> <span class='badge badge-warning'>" + ((cr[0].entrada_tarde == null) ? 'Pendiente' : cr[0].entrada_tarde) + " - " + ((cr[0].salida_tarde == null) ? 'Pendiente' : cr[0].salida_tarde) + "</span><br>";
                        cad_html = cad_html + "<b>Noche:</b> <span class='badge badge-warning'>" + ((cr[0].entrada_noche == null) ? 'Pendiente' : cr[0].entrada_noche) + " - " + ((cr[0].salida_noche == null) ? 'Pendiente' : cr[0].salida_noche) + "</span><br>";
                        if (cr[0].id != null) {
                            var bandera = 0;
                            var horag = moment().format('H:mm:ss');
                            var horav = horag.split(":");
                            if ((horav[0] * 1) >= 8 && (horav[1] * 1) >= 11) {
                                if (cr[0].dif_ent_man == null || (cr[0].dif_ent_man * 1) < 0) {
                                    bandera = 1;
                                }
                            }

                            if (bandera == 0) {
                                if ((horav[0] * 1) >= 14 && (horav[1] * 1) >= 11) {
                                    if (cr[0].dif_ent_tar == null || (cr[0].dif_ent_tar * 1) < 0) {
                                        bandera = 1;
                                    }
                                }
                            }

                            if (bandera != 0) {
                                if (cr[0].obs_explicito == null) {
                                    cr[0].obs_explicito = "";
                                }
                                cad_html = cad_html + "<br><b>Justificación Retraso</b><br><span><textarea rows='2' id='html_justifica' name='html_justifica'>" + cr[0].obs_explicito + "</textarea></span><br>";
                                cad_html = cad_html + "<a class='badge badge-warning ' onclick='justificar_biometria_retardo_con(" + cr[0].id + ")'>Justifica</a><br>";
                            }
                        }
                        jQuery.gritter.add({title: 'Biometria', text: cad_html, image: rutaAbs + 'img/huella.png', time: '300000', class_name: 'gritter-info gritter-light'});
                    }
                }
            }
        });
    }
}
comprobar_biometria_hoy();
function justificar_biometria_retardo_con(id) {
    var justificacion = $('#html_justifica').val();
    if (justificacion == "") {
        exit();
    }

    $.ajax({type: 'POST', dataType: 'json',
        url: rutaAbs + "administrativo/biometria/guardar_hoy_horariojustificacion",
        data: "id=" + id + "&justificacion=" + justificacion, success: function (data) {
            var class_not = "";
            var msg = "";
            if (data == true) {
                class_not = 'gritter-success';
                msg = "Justificación almacenada, Correctamente";
            } else {
                class_not = 'gritter-error';
                msg = "Ocurrió un error al intentar Almacenar la Información. Intente nuevamente.";
            }
            jQuery.gritter.add({title: 'Ingana', text: msg, time: '3000', class_name: class_not});
        }
    });
}