/* hyoG Rev 29112018 <= QUE MODIFICAN CAMPOS => */
function formatear_fechas() {
    $(".f_fecha").datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd/mm/yyyy",
        language: "es"
    });
}

function FormatearChosenAP() {
    $('.chosen_AP').bind();
    $('.chosen_AP').chosen({
        allow_single_deselect: true,
        width: '100%'
    });
}

function FormatearRelog() {
    $('.clockpicker').timepicker({
        minuteStep: 5,
        showSeconds: false,
        showMeridian: true,
        template: false,
        defaultTime: false
    });
}
function FormatearSpoilers() {
    $('.spoiler').click(function () {
        var cad_id = $(this).attr("data-id");
        $("#spoiler_" + cad_id).slideToggle({duration: 250});
    });
}
/* FIN hyoG Rev 29112018 <= QUE MODIFICAN CAMPOS => */

/* hyoG Rev 29112018 <= PROCEDIMIENTOS QUE MODIFICAN LA PANTALLA => */
function FormatearFiltros() {
    $('#a_br_a, #a_br_s').parent().addClass("help-section").attr({'data-id-help': 35});
    $('#a_br_a').on('click', function () {
        $('#a_br_s').removeClass('hidden');
        $('#br_simple').addClass('hidden');
        $('#bt_simple').addClass('hidden');
        $('#a_br_a').addClass('hidden');
        $('#br_avanzada').removeClass('hidden');
        $('#bt_avanzada').removeClass('hidden');
    });
    $('#a_br_s').on('click', function () {
        $('#a_br_s').addClass('hidden');
        $('#br_simple').removeClass('hidden');
        $('#bt_simple').removeClass('hidden');
        $('#a_br_a').removeClass('hidden');
        $('#br_avanzada').addClass('hidden');
        $('#bt_avanzada').addClass('hidden');
    });

    $('#btn_filter').addClass("help-section").attr({'data-id-help': 35});

    $(document).find('#btn_avanzadas').unbind('click').on('click', function () {
        $("#div_avanzadas").slideToggle({duration: 250});
    });

    $(document).find('#btn_filter').parent().unbind('click').on('click', "#btn_filter", function () {
        $("#div_filters").slideToggle({duration: 250});
        $("#dt_filter_by_columns").slideToggle({duration: 1});
    });
    $("#dt_filter_by_columns").slideToggle({duration: 1});
}

function VolverForm() {
    $(document).unbind('keyup');
    document.title = 'Scai';
    habilitarF5();
    $("#form_content").hide();
    $("#form_content").empty();
    $("#contenedor").show("slide", {direction: "down"}, 500);
    var self = $("body");
    self.css('background-image', 'url(' + rutaAbs + 'img/background/' + ref_img + ')');
    self.css('background-image', 'no-repeat scroll 0% 0% / 100% 100%');
    //FunctionFormSecundario = null;
    //MinimizarChats();
}

function IrForm() {
    $(document).unbind('keyup');
    document.title = 'Scai';
    habilitarF5();
    $("#contenedor").hide();
    $("#form_content").show("slide", {direction: "down"}, 500);
    var self = $("body");
    //MinimizarChats();
}

function politica_datos(OP) {
    if (OP) {
        $('.btn-politcas').on('click', function () {
            var new_value = $(this).val();
            if (new_value != glo_politicas) {
                $.ajax({
                    data: {pl_datos: new_value}, type: 'POST', dataType: 'json',
                    url: rutaAbs + "auxiliares/politicas",
                    async: false, global: false,
                    success: function (data, textStatus, jqXHR) {
                        glo_politicas = new_value;
                    }
                });
            }
            $("#md_politicas").modal("hide");
        });
        $("#md_politicas").modal("show");
    }
}

function RedibujarDTOtroLimit(t, row_count) {
    var oSettings = t.fnSettings();
    oSettings._iDisplayLength = parseInt(row_count, 10);
    t._fnCalculateEnd(oSettings);
    if (oSettings.fnDisplayEnd() == oSettings.fnRecordsDisplay()) {
        oSettings._iDisplayStart = oSettings.fnDisplayEnd() - oSettings._iDisplayLength;
        if (oSettings._iDisplayStart < 0) {
            oSettings._iDisplayStart = 0;
        }
    }

    if (oSettings._iDisplayLength == -1) {
        oSettings._iDisplayStart = 0;
    }

    t.fnDraw(oSettings);
    return t;
}

var Glo_delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function ColocarTableTools(dTable) {
    var dtapi = dTable.api();
    for (var ky = 0; ky < dTable.fnSettings().aoColumns.length; ky++) {
        var titulo = dtapi.column(ky).header();
        var cad_html = "<option value='" + ky + "'>" + $(titulo).text().trim() + "</option>";
        $('#busq_avanzada').append(cad_html);
    }
    /*delay(function () {
     dtapi.columns( 1 ).search( '103' ).draw();
     }, 1000);*/

    $('#busq_ext').keyup(function () {
        Glo_delay(function () {
            dtapi.search('').columns().search('');
            if ($('#busq_avanzada').val() != "") {
                dTable.fnFilter($('#busq_ext').val(), $('#busq_avanzada').val());
            } else {
                dTable.fnFilter($('#busq_ext').val());
            }
        }, 500);
    });

    $('#cant_reg').on('change', function () {
        RedibujarDTOtroLimit(dTable, $(this).val());
    });

    var tableTools_obj = new $.fn.dataTable.TableTools(dTable, {
        "sSwfPath": "<?= base_url(); ?>resources/assets/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
        "aButtons": [
            {
                "sExtends": "copy",
                "sToolTip": "Copiar al Portapapeles",
                "sButtonClass": "btn btn-xs btn-white btn-primary btn-bold btn-round",
                "sButtonText": "<i class='fa fa-copy bigger-110 pink'></i>",
                "fnComplete": function () {
                    var cad_html = '<h3 class="no-margin-top smaller">Tabla Copiada</h3><p> los registros fueron copiados al portapapeles.</p>';
                    jQuery.gritter.add({text: cad_html, time: '3000', class_name: 'gritter-success gritter-light gritter-center'});
                }
            },
            {
                "sExtends": "csv",
                "sToolTip": "Exportar a CSV",
                "sButtonClass": "btn btn-xs btn-white btn-primary  btn-bold btn-round",
                "sButtonText": "<i class='fa fa-file-excel-o bigger-110 green'></i>"
            },
            {
                "sExtends": "pdf",
                "sToolTip": "Exportar a PDF",
                "sButtonClass": "btn btn-xs btn-white btn-primary  btn-bold btn-round",
                "sButtonText": "<i class='fa fa-file-pdf-o bigger-110 red'></i>"
            },
            {
                "sExtends": "print",
                "sToolTip": "Vista Previa",
                "sButtonClass": "btn btn-xs btn-white btn-primary  btn-bold btn-round",
                "sButtonText": "<i class='fa fa-print bigger-110 grey'></i>",
                "sMessage": "<div class='navbar navbar-default'><div class='navbar-header'><span class='navbar-brand'><button class='btn btn-xs btn-round btn-success' type='button' onclick='window.print()'><i class='fa fa-print'></i> Imprimir </button></span></div></div>",
                "sInfo": function () {
                    var cad_html = "<h3 class='no-margin-top'>Vista Previa</h3><p>Por favor use su navegador para imprimir esta tabla.<br>Pulse <b>escape</b> para regresar.</p>";
                    jQuery.gritter.add({text: cad_html, time: '7000', class_name: 'gritter-success gritter-light gritter-center'});
                }
            }
        ]
    });

    $(".btns_export").each(function () {
        if ($(this).html() == "") {
            $(this).addClass("help-section").attr({'data-id-help': 36});
            $(tableTools_obj.fnContainer()).appendTo($(this));
        }
    });
    setTimeout(function () {
        $(tableTools_obj.fnContainer()).find('a.DTTT_button').each(function () {
            var div = $(this).find('> div');
            if (div.length > 0)
                div.tooltip({container: 'body'});
            else
                $(this).tooltip({container: 'body'});
        });
    }, 200);
}

/* FIN hyoG Rev 29112018 <= PROCEDIMIENTOS QUE MODIFICAN LA PANTALLA => */


/* hyoG Rev 29112018 <= MODALES => */
function ShowGeneralOptions() {
    $("#opciones_generales").trigger("click");
    $('#div_opcs_sesion').slideUp();
    $('#div_opcs_quick_access').slideDown();
}

function ShowSessionOptions() {
    $("#opciones_generales").trigger("click");
    $('#div_opcs_sesion').slideDown();
    $('#div_opcs_quick_access').slideUp();
}

function ShowErrorLogs() {
    $("#dialog_principal").html($("#ErrorLog").html());
    $('#ErrorReport').find('#container').ace_scroll({size: 400, styleClass: 'scroll-dark scroll-thin'});
    $("#dialog_principal").removeClass('hide').dialog({
        modal: true, title_html: true, width: 500, height: 260,
        title: "<span class='smaller text-success bolder'><i class='ace-icon fa fa-bug'></i> Historial de insconsistencias </span>",
        close: function (event, ui) {
            $(this).dialog("destroy").addClass('hide').empty();
        }
    });
}

function OcultarAyuda() {
    $('#btn_cerrar_ayuda').addClass('hide');
    $('#btn_ayuda_whatsapp').addClass('hide');
    $('#panel_ayuda').removeClass('col-lg-3').hide();
    $('#espacio_trabajo').removeClass('col-lg-9').addClass('col-lg-12');
}

function AbrirPanelAyuda() {
    $('#panel_ayuda').addClass('col-lg-3').show();
    $('#espacio_trabajo').removeClass('col-lg-12').addClass('col-lg-9');
    $('#btn_cerrar_ayuda').removeClass('hide');
    $('#btn_ayuda_whatsapp').removeClass('hide');
}

function AbrirWPSoporte() {
    let url_wp_soporte = "https://api.whatsapp.com/send?phone=+573107930691";
    window.open(url_wp_soporte, '_blank');
}
/* FIN hyoG Rev 29112018 <= MODALES => */


/* Rev 29112018 <= CON WINDOW COMO ARRANQUE => */
window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
    if (!(errorMsg.indexOf("'form' of undefined") + 1) && !(errorMsg.indexOf(': a i') + 1) && !(url.indexOf('cometchat') + 1) && !(url.indexOf('TableTools') + 1) && !(url.indexOf('Auditoria/jx_registrar_log') + 1)) {
        $('.page-content').unblock();
        $('#icon_bugs').removeClass('green');
        $('#icon_bugs').addClass('orange');
        $("#ErrorLog").html($("#ErrorLog").html() + "<span class='small'><b>" + url + "</b><br>" + errorObj + "</span>");
        $("#ErrorLog").find('style').remove('style');
        document.getElementById('sonido_not').play();

        var Obj_audi_error = {
            msg: errorMsg,
            url: url,
            obj_err: errorObj,
            base: GLO_js_storage_id,
            user: GLO_js_user_id
        };

        $.ajax({data: Obj_audi_error, type: 'POST', url: URL_AUDI_LOG});
    }
}