/* hyoG Rev 29112018 <= MANEJO DE NOTIFICACIONES DEL SISTEMA => 
 *  VERIFICAR QUE TAN OBSOLETO ESTA TODO ESTO*/

sessionStorage.setItem("VistoTareas", "NO");
sessionStorage.setItem("PAsVistos", ",");
var MostrarNotifi = true;
function OcultarNotificaciones() {
    if (MostrarNotifi) {
        MostrarNotifi = false;
        $('#icon_notificaciones').removeClass('fa-bell-slash');
        $('#icon_notificaciones').addClass('fa-bell');
        $('#prefijo_notificaciones').html('Mostrar');
    } else {
        MostrarNotifi = true;
        $('#icon_notificaciones').addClass('fa-bell-slash');
        $('#icon_notificaciones').removeClass('fa-bell');
        $('#prefijo_notificaciones').html('Ocultar');
    }
}

function generar_notificaciones(prio, msg, title, ruta, id) {
    if (Notification && MostrarNotifi && !isMovil) {
        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        }
        var title = (title == "") ? "Ingana" : title;
        var cad_ruta = "";
        switch (prio) {
            case "Alta":
                cad_ruta = "img/botones/hoja_danger.png";
                break;
            case "Media":
                cad_ruta = "img/botones/hoja_warning.png";
                break;
            case "Baja":
                cad_ruta = "img/botones/hoja_success.png";
                break;
            default:
                cad_ruta = "img/botones/hoja.png";
        }

        var extra = {
            icon: rutaAbs + cad_ruta,
            body: msg
        }


        var noti = new Notification(title, extra);
        noti.onclick = function () {
            if (ruta != "") {
                window.focus();
                //REDIRECCIONANDO
                switch (ruta) {
                    case "administrativo/informes/detalle_gestion":
                        IrEvaluacionDesempeno();
                        break;
                    case "administrativo/actividades/list_cro_usu":
                        sessionStorage.setItem("VistoTareas", "SI");
                        $.ajax({
                            url: rutaAbs + "" + ruta, type: 'POST',
                            success: function (data) {
                                $("#contenedor").html(data);
                                VolverForm();
                            }
                        });
                        break;
                    case "administrativo/actividades/admin_pa":
                        sessionStorage.setItem("PAsVistos", id + "," + sessionStorage.getItem("PAsVistos"));
                        ruta = ruta + "/" + id;
                        $.ajax({
                            url: rutaAbs + "" + ruta, type: 'POST',
                            success: function (data) {
                                $("#form_content").html(data);
                                IrForm();
                            }
                        });
                        break;
                    case "c_comunicacion/anuncio/listar_anuncio":
                        $.ajax({
                            url: rutaAbs + ruta, type: 'POST',
                            success: function (data) {
                                $("#form_content").html(data);
                                IrForm();
                                setTimeout(function () {
                                    $('#anuncio_enc_' + id).trigger('click');
                                }, 500);

                            }
                        });
                        break;
                }
            }
        }
        noti.onclose = {
// Al cerrar
        }
        setTimeout(function () {
            noti.close();
        }, 10000);
        document.getElementById('sonido_not').play();
    }
}

var n_sync = 0;
function guardia_sesion() {
    humanizar_horas();
    var VT = sessionStorage.getItem("VistoTareas");
    var cad_PAS = sessionStorage.getItem("PAsVistos");
    var ha = moment();
    /* CADA 5 MINUTOS */
    var min_act = ha.minute();
    var res = (min_act % 5);
    if ((res == 0 && BandSession) || n_sync == 0) {
        /* BUSCAR NOTIFICACIONES DE LA AGENDA PERSONAL. */
        $.ajax({
            type: "POST", dataType: "json",
            url: rutaAbs + "administrativo/agenda/get_notificacion_agenda",
            beforeSend: function () {
                $('.page-content').unblock();
            },
            success: function (data) {

                if (data.nots_agenda.length > 0) {
                    var LimSup = data.nots_agenda.length;
                    for (var j = 0; j < LimSup; j++) {
                        var i = 0;
//
                        var desde = moment(data.nots_agenda[j].fechaini + ' ' + data.nots_agenda[j].hi);
                        var hasta = moment(data.nots_agenda[j].fechafin + ' ' + data.nots_agenda[j].hf);
                        while (moment(desde).isBefore(hasta)) {
                            if (moment(ha.format("YYYY-MM-DD HH:mm")).isSame(desde.format("YYYY-MM-DD HH:mm"))) {
                                generar_notificaciones("", data.nots_agenda[j].msg, "Agenda", "administrativo/agenda/admin_eventos/", data.nots_agenda[j].id);
                            }
                            i++;
                            var pivot = data.nots_agenda[j].periodo_not_mit * i;
                            desde = moment(data.nots_agenda[j].fechaini + ' ' + data.nots_agenda[j].hi).add(pivot, 'm');
                        }
                    }
                }
            }
        });
        //ACTUALIZAR NOTIFICACIONES
        var cad_notis = "";
        if ($('#badg_anuns').length || $('#bs_badg_anun_doc').length || $('#bs_badg_anun_est').length || $('#ml_badg_anun_est').length || $('#ml_badg_anun_doc').length) {
            cad_notis += ",anuncios";
        }
        if ($('#badg_notis').length) {
            cad_notis += ",notificaciones";
        }
        /* if ($('#badg_activs').length) {
         cad_notis += ",actividades";
         } */
        if ($('#badg_agenda').length) {
            cad_notis += ",agenda";
        }

        $.ajax({
            type: "POST", dataType: "json", data: "cad_notis=" + cad_notis,
            url: rutaAbs + "c_maestros/notificaciones/master_notificaciones",
            beforeSend: function () {
                $('.page-content').unblock();
            },
            success: function (data) {
                if (data.n_news_update.length > 0) {
                    var nots = (data.n_news_update[0].cant_upd * 1);
                    nots = (nots == 0) ? "" : nots;
                    $('#badg_updates_scai').html(nots);
                }

                if (cad_notis.indexOf("anuncios") >= 0) {
                    var nots = (data.anuns * 1);
                    nots = (nots == 0) ? "" : nots;
                    $('#badg_anuns').html(nots);
                    if ($('#bs_badg_anun_doc').length) {
                        $('#bs_badg_anun_doc').html(nots);
                    }
                    if ($('#bs_badg_anun_est').length) {
                        $('#bs_badg_anun_est').html(nots);
                    }
                    if ($('#ml_badg_anun_est').length) {
                        $('#ml_badg_anun_est').html(nots);
                    }
                    if ($('#ml_badg_anun_doc').length) {
                        $('#ml_badg_anun_doc').html(nots);
                    }
                    var cad_html = '';
                    /* if (nots > 0) {
                     if ((data.anuns.news_anun * 1) > 0 || (data.anuns.news_come * 1) > 0) {
                     var aux_html = ((data.anuns.news_anun * 1) > 0) ? '<span class="blue">' + data.anuns.news_anun + ' Anuncios</span>' : '';
                     aux_html += (aux_html != '' && (data.anuns.news_come * 1) > 0) ? ' y <span class="blue">' + data.anuns.news_come + ' Comentarios</span>' : '<span class="blue">' + data.anuns.news_come + ' Comentarios</span>';
                     
                     cad_html += '<li class="dropdown-content">\
                     <ul class="dropdown-menu dropdown-navbar">\
                     <li>\
                     <a class="notis_a_enlaces" href="c_comunicacion/anuncio/listar_anuncio" data-id="0">\
                     <span class="msg-title">\
                     Se han creado ' + aux_html + ' Nuevos en el tablero de anuncios.\
                     </span>\
                     </a>\
                     </li>\
                     </ul>\
                     </li>';
                     }
                     }
                     cad_html += '<li class="dropdown-footer"><a class="notis_a_enlaces" href="c_comunicacion/anuncio/listar_anuncio" data-id="0">Ver Muro</a></li>';
                     $('#list_anuns').html(cad_html); */
                }

                if (cad_notis.indexOf("notificaciones") >= 0) {
                    var nots = (data.tot_notis * 1);
                    nots = (nots == 0) ? "" : nots;
                    $('#badg_notis').html(nots);
                    var cad_html = '';
                    if (nots > 0) {
                        cad_html = '<li class="dropdown-header">Ultimas Asignaciones</li>';
                        var LimSup = data.notis.length;
                        for (var j = 0; j < LimSup; j++) {
                            cad_html += '<li class="dropdown-content">\
                                                            <ul class="dropdown-menu dropdown-navbar">\
                                                            <li>\
                                                                <a class="notis_a_enlaces" href="' + data.notis[j].abrir_con + '" data-id="' + data.notis[j].id + '">\
                                                                    <span class="msg-title">\
                                                                        <span class="blue">' + data.notis[j].usuario + '</span>\
                                                                        Actualizo el <span class="blue">' + data.notis[j].instrumento + ':</span><br>' + data.notis[j].descripcion + '</span>\
                                                                        <span class="msg-time">\
                                                                        <i class="ace-icon fa fa-clock-o"></i>\
                                                                        <span class="humanizar">\
                                                                            <span id="mostrar"></span>\
                                                                            <span id="calcular" class="hidden">' + data.notis[j].fecha_system + '</span>\
                                                                        </span>\
                                                                    </span>\
                                                                    </a>\
                                                                    </li>\
                                                        </ul>\
                                                    </li>';
                        }
                    } else {
                        cad_html += '<li class="dropdown-content"><ul class="dropdown-menu dropdown-navbar">\
                                                            <li>No tienes asignaciones nuevas.</li></ul></li>';
                    }
                    /* cad_html += '<li class="dropdown-footer"><a href="#">Ver Historial<i class="ace-icon fa fa-arrow-right"></i></a></li>'; */
                    $('#list_notis').html(cad_html);
                }

                if (cad_notis.indexOf("agenda") >= 0) {
                    var nots = (data.agends.length * 1);
                    nots = (nots == 0) ? "" : nots;
                    $('#badg_agenda').html(nots);
                    var cad_html = '';
                    if (nots > 0) {
                        cad_html = '<li class="dropdown-header">Agenda de Hoy <button class="btn btn-minier btn-success btn-round bs_a_enlaces pull-right" href="administrativo/agenda/agenda_usu">Mi Agenda</button></li>';
                        var LimSup = data.agends.length;
                        for (var j = 0; j < LimSup; j++) {
                            var desc = (data.agends[j].descripcion == null) ? "" : data.agends[j].descripcion;
                            cad_html += '<li class="dropdown-content">\
                                <ul class="dropdown-menu dropdown-navbar">\
                                <li>\
                                    <a class="bs_a_enlaces" href="administrativo/agenda/agenda_usu">\
                                        <span class="msg-title">\
                                            <span class="blue">' + data.agends[j].name + '</span><br>' + desc + '\
                                            <span class="msg-time">\
                                            <i class="ace-icon fa fa-clock-o"></i>\
                                            <span class="humanizar">\
                                                <span class="blue">Inicia</span> <span id="mostrar"></span>\
                                                <span id="calcular" class="hidden">' + data.agends[j].fechaini + ' ' + data.agends[j].horaini + '</span>\
                                            </span>\
                                        </span>\
                                        </a>\
                                        </li>\
                            </ul>\
                        </li>';
                        }
                    } else {
                        cad_html += '<li class="dropdown-content"><ul class="dropdown-menu dropdown-navbar">\
                                                            <li>Hoy no tienes recordatorios.</li></ul></li>';
                    }
                    cad_html += '<li class="dropdown-footer"><a class="bs_a_enlaces" href="administrativo/agenda/agenda_usu" align="right">\
                                    Mi Agenda <i class="ace-icon fa fa-arrow-right"></i></a></li>';
                    $('#list_agenda').html(cad_html);
                }

                humanizar_horas();
                formatear_enlaces_notis();
            }
        });
    }
    /* FIN CADA 5 MINUTOS */

    //CADA HORA EN EL MINUTO 0
    var min_act = ha.minute();
    var hor_act = ha.hour();
    if (min_act == 0 && BandSession) {
        //GENERAR ALERTAS DE LOS CRONOGRAMAS
        $.ajax({
            type: "POST", dataType: "json", data: "hora=" + hor_act,
            url: rutaAbs + "administrativo/agenda/get_notificacion_tareas",
            beforeSend: function () {
                $('.page-content').unblock();
            },
            success: function (data) {
                if (data.nots_tareas.length > 0) {
                    var LimSup = data.nots_tareas.length;
                    for (var j = 0; j < LimSup; j++) {
                        generar_notificaciones(data.nots_tareas[j].name, data.nots_tareas[j].msg, "Tareas que vencen Hoy", "", "");
                    }
                }
            }
        });
    }

    //TODOS LOS DIAS A LAS 11
    var min_act = ha.minute();
    var hor_act = ha.hour();
    if (hor_act == 11 && min_act == 0 && BandSession) {
        // EVALUACION DE DESEMPEÑO
        $.ajax({
            type: "POST", dataType: "json",
            url: rutaAbs + "administrativo/informes/getEvaluacionDesempenoActual",
            beforeSend: function () {
                $('.page-content').unblock();
            },
            success: function (data) {
                //var msg = "Total AT" + data.total[0].cant + " Total AM" + data.mensual[0].cant;
                var msg = "Este mes has cumplido con " + data.mensual[0].actcump + " de " + data.mensual[0].cant + " actividades asignadas. Tu esfuerzo es vital para nuestra empresa. Sigue Adelante!!";
                generar_notificaciones("", msg, "Felicitaciones", "administrativo/informes/detalle_gestion", "");
            }
        });
    }

    //CADA HORA EN EL MINUTO 30
    var min_act = ha.minute();
    if (min_act == 30 && VT == "NO" && BandSession) {
        //GENERAR ALERTA DE LAS TAREAS DE LA SEMANA
        $.ajax({
            type: "POST", dataType: "json",
            url: rutaAbs + "administrativo/agenda/get_tareas_semana",
            beforeSend: function () {
                $('.page-content').unblock();
            },
            success: function (data) {
                if (data.cant_tareas > 0) {
                    generar_notificaciones("Alta", data.msg, "Tareas De Esta Semana", "administrativo/actividades/listar_mis_tareas", "");
                }
            }
        });
    }

    //CADA HORA EN EL MINUTO 15
    var min_act = ha.minute();
    //and cad_PAS!="All"
    if (min_act == 15 && BandSession) {
        //GENERAR UNA ALERTA DE LOS PLANES DE ACCION PENDIENTES
        $.ajax({
            type: "POST", dataType: "json",
            url: rutaAbs + "administrativo/agenda/get_progreso_pas",
            beforeSend: function () {
                $('.page-content').unblock();
            },
            success: function (data) {
                var arr_PAS = cad_PAS.split(",");
                for (var i = 0; i < data.cr_pas.length; i++) {
                    if ($.inArray(data.cr_pas[i].id, arr_PAS) < 0) {
                        var porc_pas = (data.cr_pas[i].cant_log * 100) / data.cr_pas[i].cant_met;
                        if ((data.cr_pas[i].dif_ult_dia * 1) < 0) {
                            var msg = "Su Plan de Acción termino " + moment(data.cr_pas[i].rangofin, 'YYYY-MM-DD').locale('es').fromNow() + " y su progreso es de " + porc_pas.toFixed(2) + " %";
                            generar_notificaciones("Alta", msg, data.cr_pas[i].titulo, "administrativo/actividades/admin_pa", data.cr_pas[i].id);
                        } else {
                            var porc_fec = (data.cr_pas[i].dif_ult_dia * 100) / data.cr_pas[i].tot_dias;
                            if (porc_fec < porc_pas) {
                                var msg = "Su Plan de Acción termina " + moment(data.cr_pas[i].rangofin, 'YYYY-MM-DD').locale('es').fromNow() + " y su progreso es de " + porc_pas.toFixed(2) + " %";
                                generar_notificaciones("Media", msg, data.cr_pas[i].titulo, "administrativo/actividades/admin_pa", data.cr_pas[i].id);
                            }
                        }
                    }
                }
            }
        });
    }
    n_sync++;
    /* setTimeout('guardia_sesion()', 60000); */
}

guardia_sesion();

function formatear_enlaces_notis() {
    $(".notis_a_enlaces").unbind('click').on("click", function (e) {
        e.preventDefault();
        var self = $(this);
        if (self.attr("data-id") != "0") {
            $.ajax({url: rutaAbs + "c_maestros/notificaciones/set_visto/" + self.attr("data-id"), type: 'POST'});
        }
        $.ajax({
            url: rutaAbs + "" + self.attr("href"), type: 'POST',
            success: function (data) {
                $("#form_content").html(data);
                IrForm();
            }
        });
    });
    $(".bs_a_enlaces").unbind('click').on("click", function (e) {
        e.preventDefault();
        var self = $(this);
        $.ajax({
            url: rutaAbs + "" + self.attr("href"), type: 'POST',
            success: function (data) {
                VolverForm();
                $("#contenedor").html(data);
            }
        });
    });
}

formatear_enlaces_notis();