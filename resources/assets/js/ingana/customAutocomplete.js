var elementVal = [];
function customAutocomplete(params){
    var ajax = params.ajax;
    var label = params.label;
    var id = params.id;
    var url = ajax.url;
    params.minLength = (params.minLength) ? params.minLength : 0;
    var data = {};

    elementVal[label] = id;

    if(ajax.data){
        data = ajax.data();
    }

    var temp_data = [];
    var busq = false;

    /*if(params.init) params.init($(label).val());*/

    if($(label).val() != "" && $(label).val()){
        $.ajax({
            dataType: 'json',
            type : 'POST',
            url: url + $(label).val(),
            async: true,
            success: function(data) {
                var text = (data.input) ? data.input : data.label;
                $(label).val(text);

                $(id).val(data.value);

                if(params.select) params.select(data);
            },
        })
    }

    $(label).autocomplete({
        delay: params.delay ? params.delay : 500,
        minLength: params.minLength,
        source: function (request, response){
            $(id).val('');
            data['term'] = request.term
            temp_data = [];
            $.ajax({
                dataType: 'json',
                type : 'POST',
                url: url,
                async: true,
                data: data,
                success: function(data) {
                    temp_data = data;
                    busq = true;
                    response(data);
                },
            })
        },
        focus: function( event, ui ) {
            event.preventDefault();

            var text = (ui.item.input) ? ui.item.input : ui.item.label;
            $(label).val(text);
        },
        select: function( event, ui ) {
            event.preventDefault();

            temp_data = [];

            var text = (ui.item.input) ? ui.item.input : ui.item.label;
            $(label).val(text);

            $(id).val(ui.item.value);

            if(params.select) params.select(ui.item);

            busq = false;
        },
        search: function() {
            $(id).val('');
        },
        /*change: function (event, ui){
            event.preventDefault();
            var data_change = {};
            if($(id).val() == "" && !(temp_data.length == 0 || !busq) && ui.item != null){
                data['term'] = request.term
                $.ajax({
                    dataType: "json",
                    type : 'POST',
                    url: url,
                    data: data,
                    async: true,
                    success: function(data) {

                        if(data.length > 0) {
                            var ex = false;
                            for (var i = 0; i < data.length; i++) {
                                if (data[i].label.toLowerCase() == $(label).val().toLowerCase()) {
                                    var text = (data[i].input) ? data[i].input : data[i].label;
                                    $(label).val(text);
                                    $(id).val(data[i].value);
                                    data_change = data[i];
                                    ex = true;
                                }
                            }

                            if (!ex) {
                                var text = (data[0].input) ? data[0].input : data[0].label;
                                $(label).val(text);
                                $(id).val(data[0].value);
                                data_change = data[0];
                            }
                        }else{
                            $(label).attr( "title", "Opción no válida" ).tooltip().focus();
                            $(id).val('');

                            setTimeout(function() {
                                $(label).tooltip( "close" ).attr( "title", "" );
                            }, 2200);

                        }
                    },
                });
            }else if($(id).val() == "" && temp_data.length > 0 && ui.item == null){

                var ex = false;
                for(var i = 0; i < temp_data.length; i++){
                    if(temp_data[i].label.toLowerCase() == $(label).val().toLowerCase()){
                        var text = (temp_data[i].input) ? temp_data[i].input : temp_data[i].label;
                        $(label).val(text);
                        $(id).val(temp_data[i].value);
                        data_change = temp_data[i];
                        ex = true;
                    }
                }

                if(!ex){
                    var text = (temp_data[0].input) ? temp_data[0].input : temp_data[0].label;
                    $(label).val(text);
                    $(id).val(temp_data[0].value);
                    data_change = temp_data[0];
                }

            }else if($(id).val() == "" && ui.item == null) {
                $(label).attr("title", "Opción no válida").tooltip().focus();
                $(id).val('');

                setTimeout(function () {
                    $(label).tooltip("close").attr("title", "");

                }, 2200);
            }else if (temp_data.length > 0){

                var ex = false;
                for(var i = 0; i < temp_data.length; i++){
                    if(temp_data[i].label.toLowerCase() == $(label).val().toLowerCase()){
                        var text = (temp_data[i].input) ? temp_data[i].input : temp_data[i].label;
                        $(label).val(text);
                        $(id).val(temp_data[i].value);
                        data_change = temp_data[i];
                        ex = true;
                    }
                }

                if(!ex){
                    var text = (temp_data[0].input) ? temp_data[0].input : temp_data[0].label;
                    $(label).val(text);
                    $(id).val(temp_data[0].value);
                    data_change = temp_data[0];
                }
            }


            if(params.select) params.select(data_change)

            temp_data = [];
            busq = false;
        },*/

    }).one('focus',function(){
        if (params.minLength == 0){
            $(label).autocomplete( "search", $(label).val());
        }
    });

    /*$(label).focusout(function(){
        console.log($(id).val())
        if($(id).val() == "" && temp_data.length > 0) {
            var ex = false;
            for(var i = 0; i < temp_data.length; i++){
                if(temp_data[i].label.toLowerCase() == $(label).val().toLowerCase()){
                    var text = (temp_data[i].input) ? temp_data[i].input : temp_data[i].label;
                    $(label).val(text);
                    $(id).val(temp_data[i].value);
                    ex = true;
                }
            }

            if(!ex){
                var text = (temp_data[0].input) ? temp_data[0].input : temp_data[0].label;
                $(label).val(text);
                $(id).val(temp_data[0].value);
            }
        }
    });*/

    $('.ui-autocomplete').css({ 'max-height': (params.maxHeight) ? params.maxHeight: '250px' })
        .css({'overflow-y': 'auto' })
        .css({'overflow-x': 'hidden'})
        .css({'padding-right': '20px'});

    $(label).css({'height': '30px'})

    if(params.required){
        $.validator.addMethod("required_" + id.replace('#', ''), function(value, element) {

            //console.log($(element).attr('id'))
            var input_id = elementVal['#'+$(element).attr('id')];

            if($(input_id).val() == "") return false;

            return true;
        }, "Este campo es requerido");

        setTimeout(function (){
            try {
                $(label).rules("add", "required_" + id.replace('#', ''));
            }catch (e) {

            }
        }, 500);
    }
}