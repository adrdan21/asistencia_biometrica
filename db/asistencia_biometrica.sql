/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.21 : Database - asistencia_biometrica
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`asistencia_biometrica` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `asistencia_biometrica`;

/*Table structure for table `acaasignaturadocente` */

DROP TABLE IF EXISTS `acaasignaturadocente`;

CREATE TABLE `acaasignaturadocente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periodos_id` int(11) DEFAULT NULL,
  `bancohojasvida_id` int(11) DEFAULT NULL,
  `planestudiosasignatura_id` int(11) DEFAULT NULL,
  `grupos_id` int(11) DEFAULT NULL,
  `ih` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `periodos_id` (`periodos_id`),
  KEY `bancohojasvida_id` (`bancohojasvida_id`),
  KEY `planestudiosasignatura_id` (`planestudiosasignatura_id`),
  KEY `grupos_id` (`grupos_id`),
  CONSTRAINT `acaasignaturadocente_ibfk_1` FOREIGN KEY (`periodos_id`) REFERENCES `acaperiodos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acaasignaturadocente_ibfk_2` FOREIGN KEY (`bancohojasvida_id`) REFERENCES `bancohojasvida` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acaasignaturadocente_ibfk_3` FOREIGN KEY (`planestudiosasignatura_id`) REFERENCES `acaplanestudiosasignaturas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acaasignaturadocente_ibfk_4` FOREIGN KEY (`grupos_id`) REFERENCES `acagrupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `acaasignaturadocente` */

LOCK TABLES `acaasignaturadocente` WRITE;

insert  into `acaasignaturadocente`(`id`,`periodos_id`,`bancohojasvida_id`,`planestudiosasignatura_id`,`grupos_id`,`ih`) values (1,1,1,1,1,4),(2,1,1,2,1,4);

UNLOCK TABLES;

/*Table structure for table `acaasignaturas` */

DROP TABLE IF EXISTS `acaasignaturas`;

CREATE TABLE `acaasignaturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) DEFAULT NULL,
  `nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `acaasignaturas` */

LOCK TABLES `acaasignaturas` WRITE;

insert  into `acaasignaturas`(`id`,`codigo`,`nombre`) values (1,'LogMat','Logica Matematica'),(2,'Sim','Simulacion');

UNLOCK TABLES;

/*Table structure for table `acagrupos` */

DROP TABLE IF EXISTS `acagrupos`;

CREATE TABLE `acagrupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) DEFAULT NULL,
  `abrev` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `acagrupos` */

LOCK TABLES `acagrupos` WRITE;

insert  into `acagrupos`(`id`,`nombre`,`abrev`) values (1,'Grupo A','A'),(2,'Grupo B','B');

UNLOCK TABLES;

/*Table structure for table `acainscripcion` */

DROP TABLE IF EXISTS `acainscripcion`;

CREATE TABLE `acainscripcion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'num cedula tarjeta identidad',
  `persona_id` int(11) DEFAULT NULL COMMENT 'datos personales  del  alumno tabla  persona',
  `planestudios_id` int(11) DEFAULT NULL COMMENT 'programa  elegido ligado a un plan de estudios',
  `periodo_id` int(11) DEFAULT NULL,
  `fechaInscrito` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `estadoalumno_id` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `persona_id` (`persona_id`),
  KEY `planestudios_id` (`planestudios_id`),
  KEY `periodo_id` (`periodo_id`),
  KEY `estadoalumno_id` (`estadoalumno_id`),
  CONSTRAINT `acainscripcion_ibfk_1` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acainscripcion_ibfk_2` FOREIGN KEY (`planestudios_id`) REFERENCES `acaplanestudios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acainscripcion_ibfk_7` FOREIGN KEY (`periodo_id`) REFERENCES `acaperiodos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `acainscripcion` */

LOCK TABLES `acainscripcion` WRITE;

insert  into `acainscripcion`(`id`,`persona_id`,`planestudios_id`,`periodo_id`,`fechaInscrito`,`estadoalumno_id`) values (1,2,1,1,'2021-05-15 17:10:15',2),(2,3,2,1,'2021-05-15 17:10:24',2);

UNLOCK TABLES;

/*Table structure for table `acajornada` */

DROP TABLE IF EXISTS `acajornada`;

CREATE TABLE `acajornada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `sigla` char(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `acajornada` */

LOCK TABLES `acajornada` WRITE;

insert  into `acajornada`(`id`,`nombre`,`sigla`) values (8,'Nocturno','N');

UNLOCK TABLES;

/*Table structure for table `acamatricula` */

DROP TABLE IF EXISTS `acamatricula`;

CREATE TABLE `acamatricula` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `inscripcion_id` bigint(11) DEFAULT NULL,
  `semestre_id` int(11) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  `periodo_id` int(11) DEFAULT NULL,
  `sede_id` int(11) DEFAULT NULL,
  `jornada_id` int(11) DEFAULT NULL,
  `activo` int(1) DEFAULT '1',
  `fechaMatricula` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `retirado` tinyint(1) DEFAULT '0',
  `fecha_retiro` date DEFAULT NULL,
  `motivo_retiro` enum('Retiro','Cambio grupo','Promoción anticipada') DEFAULT 'Retiro',
  `aprobo` tinyint(1) DEFAULT NULL COMMENT 'Aprobo=1, Reprobo=0',
  PRIMARY KEY (`id`),
  KEY `semestre_id` (`semestre_id`),
  KEY `grupo_id` (`grupo_id`),
  KEY `inscripcion_id` (`inscripcion_id`),
  KEY `periodo_id` (`periodo_id`),
  CONSTRAINT `acamatricula_ibfk_1` FOREIGN KEY (`semestre_id`) REFERENCES `acasemestres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acamatricula_ibfk_2` FOREIGN KEY (`grupo_id`) REFERENCES `acagrupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acamatricula_ibfk_3` FOREIGN KEY (`inscripcion_id`) REFERENCES `acainscripcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acamatricula_ibfk_4` FOREIGN KEY (`periodo_id`) REFERENCES `acaperiodos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `acamatricula` */

LOCK TABLES `acamatricula` WRITE;

insert  into `acamatricula`(`id`,`codigo`,`inscripcion_id`,`semestre_id`,`grupo_id`,`periodo_id`,`sede_id`,`jornada_id`,`activo`,`fechaMatricula`,`retirado`,`fecha_retiro`,`motivo_retiro`,`aprobo`) values (1,'0001',1,1,1,1,1,1,1,'2021-05-15 17:11:39',0,NULL,'Retiro',NULL),(2,'0002',2,10,1,1,1,1,1,'2021-05-15 17:11:55',0,NULL,'Retiro',NULL);

UNLOCK TABLES;

/*Table structure for table `acamatriculaasignatura` */

DROP TABLE IF EXISTS `acamatriculaasignatura`;

CREATE TABLE `acamatriculaasignatura` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `matricula_id` bigint(20) DEFAULT NULL,
  `planestudiosasignatura_id` int(11) DEFAULT NULL COMMENT 'Asigantura por plan cuado  no  existe  aun docente',
  `asignaturadocente_id` int(11) DEFAULT NULL COMMENT 'Asignatura por docente',
  PRIMARY KEY (`id`),
  KEY `matricula_id` (`matricula_id`),
  KEY `asignaturadocente_id` (`asignaturadocente_id`),
  KEY `planestudiosasignatura_id` (`planestudiosasignatura_id`),
  CONSTRAINT `acamatriculaasignatura_ibfk_1` FOREIGN KEY (`matricula_id`) REFERENCES `acamatricula` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acamatriculaasignatura_ibfk_2` FOREIGN KEY (`asignaturadocente_id`) REFERENCES `acaasignaturadocente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acamatriculaasignatura_ibfk_3` FOREIGN KEY (`planestudiosasignatura_id`) REFERENCES `acaplanestudiosasignaturas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `acamatriculaasignatura` */

LOCK TABLES `acamatriculaasignatura` WRITE;

insert  into `acamatriculaasignatura`(`id`,`matricula_id`,`planestudiosasignatura_id`,`asignaturadocente_id`) values (1,1,1,1),(2,2,2,2);

UNLOCK TABLES;

/*Table structure for table `acaparametricasnotas` */

DROP TABLE IF EXISTS `acaparametricasnotas`;

CREATE TABLE `acaparametricasnotas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periodo_id` int(11) DEFAULT NULL,
  `orden_subperiodo` int(1) DEFAULT '1',
  `peso` int(3) DEFAULT NULL COMMENT 'dado  en porcentajes',
  `activo` char(1) DEFAULT '0' COMMENT 'activo 1, no activo 0',
  `fecha_inicio` date DEFAULT NULL,
  `fecha_cierre` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asignaturadocente_id` (`periodo_id`),
  CONSTRAINT `acaparametricasnotas_ibfk_2` FOREIGN KEY (`periodo_id`) REFERENCES `acaperiodos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

/*Data for the table `acaparametricasnotas` */

LOCK TABLES `acaparametricasnotas` WRITE;

insert  into `acaparametricasnotas`(`id`,`periodo_id`,`orden_subperiodo`,`peso`,`activo`,`fecha_inicio`,`fecha_cierre`) values (49,1,1,30,'1',NULL,NULL),(50,1,2,30,'0',NULL,NULL),(51,1,3,40,'0',NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `acaperiodos` */

DROP TABLE IF EXISTS `acaperiodos`;

CREATE TABLE `acaperiodos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) DEFAULT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaFin` date DEFAULT NULL,
  `sede_id` int(11) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sede_id` (`sede_id`),
  CONSTRAINT `acaperiodos_ibfk_1` FOREIGN KEY (`sede_id`) REFERENCES `acasedes` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `acaperiodos` */

LOCK TABLES `acaperiodos` WRITE;

insert  into `acaperiodos`(`id`,`descripcion`,`fechaInicio`,`fechaFin`,`sede_id`,`activo`) values (1,'2021-1','2021-02-01','2021-06-12',1,1);

UNLOCK TABLES;

/*Table structure for table `acaplanestudios` */

DROP TABLE IF EXISTS `acaplanestudios`;

CREATE TABLE `acaplanestudios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) DEFAULT NULL,
  `activo` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `acaplanestudios` */

LOCK TABLES `acaplanestudios` WRITE;

insert  into `acaplanestudios`(`id`,`nombre`,`activo`) values (1,'Tecnologia en Desarrollo de software',1),(2,'Ingenieria de sistemas',1);

UNLOCK TABLES;

/*Table structure for table `acaplanestudiosasignaturas` */

DROP TABLE IF EXISTS `acaplanestudiosasignaturas`;

CREATE TABLE `acaplanestudiosasignaturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planestudios_id` int(11) DEFAULT NULL,
  `asignaturas_id` int(11) DEFAULT NULL,
  `cantidad_creditos` int(11) DEFAULT '0',
  `intensidadHoraria` int(4) DEFAULT '0',
  `semestre_id` int(11) DEFAULT NULL,
  `activo` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `planestudios_id` (`planestudios_id`),
  KEY `asignaturas_id` (`asignaturas_id`),
  KEY `acaplanestudiosasignaturas_ibfk_3` (`semestre_id`),
  CONSTRAINT `acaplanestudiosasignaturas_ibfk_1` FOREIGN KEY (`planestudios_id`) REFERENCES `acaplanestudios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acaplanestudiosasignaturas_ibfk_2` FOREIGN KEY (`asignaturas_id`) REFERENCES `acaasignaturas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acaplanestudiosasignaturas_ibfk_3` FOREIGN KEY (`semestre_id`) REFERENCES `acasemestres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `acaplanestudiosasignaturas` */

LOCK TABLES `acaplanestudiosasignaturas` WRITE;

insert  into `acaplanestudiosasignaturas`(`id`,`planestudios_id`,`asignaturas_id`,`cantidad_creditos`,`intensidadHoraria`,`semestre_id`,`activo`) values (1,1,1,2,4,1,1),(2,2,2,2,4,10,1);

UNLOCK TABLES;

/*Table structure for table `acasedes` */

DROP TABLE IF EXISTS `acasedes`;

CREATE TABLE `acasedes` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `acasedes` */

LOCK TABLES `acasedes` WRITE;

insert  into `acasedes`(`id`,`nombre`) values (1,'Mocoa');

UNLOCK TABLES;

/*Table structure for table `acasemestres` */

DROP TABLE IF EXISTS `acasemestres`;

CREATE TABLE `acasemestres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `abrev` varchar(15) DEFAULT NULL,
  `orden` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `acasemestres` */

LOCK TABLES `acasemestres` WRITE;

insert  into `acasemestres`(`id`,`nombre`,`abrev`,`orden`) values (1,'Semestre I','S I',1),(2,'Semestre II','S II',2),(3,'Semestre III','S III',3),(4,'Semestre IV','S IV',4),(5,'Semestre V','S V',5),(6,'Semestre VI','S VI',6),(7,'Semestre VII','S VII',7),(8,'Semestre VIII','S VIII',8),(9,'Semestre IX','S IX',9),(10,'Semestre X','S X',10);

UNLOCK TABLES;

/*Table structure for table `bancohojasvida` */

DROP TABLE IF EXISTS `bancohojasvida`;

CREATE TABLE `bancohojasvida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personas_id` int(11) DEFAULT NULL,
  `razon_social` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fecha_sys` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `docente_persona` (`personas_id`),
  CONSTRAINT `bancohojasvida_ibfk_1` FOREIGN KEY (`personas_id`) REFERENCES `personas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `bancohojasvida` */

LOCK TABLES `bancohojasvida` WRITE;

insert  into `bancohojasvida`(`id`,`personas_id`,`razon_social`,`fecha_sys`) values (1,1,'Adriana Navia','2021-05-14 23:37:08');

UNLOCK TABLES;

/*Table structure for table `personas` */

DROP TABLE IF EXISTS `personas`;

CREATE TABLE `personas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificacion` varchar(30) NOT NULL,
  `tipoidentificacion_id` int(2) DEFAULT NULL,
  `nombre1` varchar(50) NOT NULL,
  `nombre2` varchar(50) DEFAULT NULL,
  `apellido1` varchar(50) NOT NULL,
  `apellido2` varchar(50) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `barrio` varchar(200) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fecha_nac` date DEFAULT NULL,
  `genero` enum('Femenino','Masculino') DEFAULT NULL,
  `tipo_sangre` enum('AB+','AB-','A+','A-','B+','B-','O+','O-') DEFAULT NULL,
  `biometria` blob,
  PRIMARY KEY (`id`),
  KEY `personas_tipo_ide` (`tipoidentificacion_id`),
  CONSTRAINT `personas_tipo_ide` FOREIGN KEY (`tipoidentificacion_id`) REFERENCES `tipoidentificacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `personas` */

LOCK TABLES `personas` WRITE;

insert  into `personas`(`id`,`identificacion`,`tipoidentificacion_id`,`nombre1`,`nombre2`,`apellido1`,`apellido2`,`direccion`,`barrio`,`telefono`,`celular`,`email`,`fecha_nac`,`genero`,`tipo_sangre`,`biometria`) values (1,'123456',8,'Adriana',NULL,'Navia',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'1124860178',8,'Adrian',NULL,'Galindres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'1234567891',8,'Danny',NULL,'Ortiz',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `secusers` */

DROP TABLE IF EXISTS `secusers`;

CREATE TABLE `secusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `pswd` varchar(45) NOT NULL,
  `active` varchar(1) NOT NULL,
  `personas_id` int(11) DEFAULT NULL,
  `fechasis` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_persona` (`personas_id`),
  CONSTRAINT `fk_usuario_persona` FOREIGN KEY (`personas_id`) REFERENCES `personas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1772 DEFAULT CHARSET=utf8;

/*Data for the table `secusers` */

LOCK TABLES `secusers` WRITE;

insert  into `secusers`(`id`,`login`,`pswd`,`active`,`personas_id`,`fechasis`) values (1771,'1124860178','9842dabc87ff56e49e32e5546021b9e4','1',1,'2021-05-15 10:56:42');

UNLOCK TABLES;

/*Table structure for table `tipoidentificacion` */

DROP TABLE IF EXISTS `tipoidentificacion`;

CREATE TABLE `tipoidentificacion` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `abreviatura` varchar(5) COLLATE latin1_spanish_ci DEFAULT NULL,
  `name` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `tipoidentificacion` */

LOCK TABLES `tipoidentificacion` WRITE;

insert  into `tipoidentificacion`(`id`,`abreviatura`,`name`) values (8,'CC','Cedula'),(9,'TI','Tarjeta Identidad'),(10,'CE','Cedula Extranjera');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
